//
//  Config.swift
//  WeatherWatch
//
//  Created by Thong Pham on 5/8/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import Foundation

let WEATHER_API:String = "http://api.openweathermap.org/data/2.5/weather?q="
let CITY_API:String = "https://restcountries.eu/rest/v1/all"
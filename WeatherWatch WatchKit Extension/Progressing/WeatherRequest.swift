//
//  WeatherRequest.swift
//  WeatherWatch
//
//  Created by Thong Pham on 5/9/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import Foundation
import Alamofire

class WeatherRequest {
    
    class func request(urlString : String,completion:(NSDictionary,NSDictionary) -> Void) {
        
        Alamofire.request(.GET, urlString).responseJSON() {
            (_, _, data, _) in
//            println(data)
            
            //parse Json
            if data != nil {
                var weatherArr : NSArray = data?.valueForKeyPath("weather") as! NSArray
                 var main : NSDictionary = data?.valueForKeyPath("main") as! NSDictionary
                
                if weatherArr.count > 0 {
                    var weatherDic : NSDictionary = weatherArr[0] as! NSDictionary
                    completion(weatherDic,main)
                }
            }
        }
        
    }
    
    
}
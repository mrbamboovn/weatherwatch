//
//  WeatherRequest.swift
//  WeatherWatch
//
//  Created by Thong Pham on 5/9/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import Foundation
import Alamofire

class CapitalRequest {
    
    class func request(urlString : String,completion:(NSMutableArray?) -> Void) {
        
        Alamofire.request(.GET, urlString).responseJSON() {
            (_, _, data, _) in
//            println(data)
            StoreData.capitals = "CON VA VANG"
            
            //parse Json
            if data != nil {
                var cityArr : NSMutableArray = data as! NSMutableArray
                var arr:NSMutableArray = []
                
                var main : NSDictionary
                for i in 0..<cityArr.count
                {
                     main = cityArr[i] as! NSDictionary
                    var na : String = main.valueForKey("capital") as! String
                  arr.addObject(na)
                }
                 completion(arr)
            }
        }
        
    }
    
    
}
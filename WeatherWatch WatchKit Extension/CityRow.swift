//
//  CityController.swift
//  WeatherWatch WatchKit Extension
//
//  Created by Thong Pham on 5/8/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import WatchKit

class CityRow: NSObject {

    @IBOutlet weak var imageCity: WKInterfaceImage!
    @IBOutlet weak var nameCity: WKInterfaceLabel!

}


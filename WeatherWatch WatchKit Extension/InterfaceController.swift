//
//  InterfaceController.swift
//  WeatherWatch WatchKit Extension
//
//  Created by Thong Pham on 5/8/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var cityTable: WKInterfaceTable!
    
    var cityList : NSMutableArray? = []
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        //call api
        self.handleCityRequest();
    }
    
    private func loadTableData(arr : NSMutableArray?) {

        self.cityList = arr
        
        cityTable.setNumberOfRows(self.cityList!.count, withRowType: "CityRow")
        
        for index in 0..<self.cityList!.count
        {
            var cityName = self.cityList?.objectAtIndex(index) as! String
            
            if let row = cityTable.rowControllerAtIndex(index) as? CityRow {
                row.nameCity.setTextColor(UIColor.whiteColor())
                row.nameCity.setText(cityName)
                row.imageCity.setImage(UIImage(named: "CityIcon"))
            }
        }

    }
 
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject?
    {
        let cityName = cityList?[rowIndex]
        return cityName
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }
    
    
    //Handle API Request for City
    private func handleCityRequest(){
        //call City API
        
        var info:String = CITY_API
        
        CapitalRequest.request(info, completion:{(arr:NSMutableArray?) -> Void in
            //set value to view
           self.loadTableData(arr)
        })
    }

    
    
}

//
//  StoreData.swift
//  WeatherWatch
//
//  Created by Thong Pham on 5/9/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import Foundation

class StoreData{
    
    class var capitals : String {
        get {
            var returnValue : String? = NSUserDefaults.standardUserDefaults().objectForKey("capitals") as? String
            return returnValue!
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "capitals")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
}
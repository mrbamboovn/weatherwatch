//
//  DetailWeatherInterfaceController.swift
//  WeatherWatch WatchKit Extension
//
//  Created by Thong Pham on 5/8/15.
//  Copyright (c) 2015 Thong Pham. All rights reserved.
//

import WatchKit

class DetailWeatherInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var nameCity: WKInterfaceLabel!
    @IBOutlet weak var tempMax: WKInterfaceLabel!
    @IBOutlet weak var tempMin: WKInterfaceLabel!
    @IBOutlet weak var tempAverage: WKInterfaceLabel!
    
    @IBOutlet weak var descriptionWeather: WKInterfaceLabel!
    @IBOutlet weak var imageWeather: WKInterfaceImage!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.configControl()
        
        if var na = context as? String {
            
            self.nameCity.setText(na)
            
            //call api
            self.handleAPIRequest(na)
        }
    }
    
    func configControl(){
        //name city
        self.nameCity.setTextColor(UIColor.whiteColor())
        self.nameCity.setText("")
        
        //Temp
        self.tempMax.setText("")
        self.tempMin.setText("")
        self.tempAverage.setText("")
        
        //description
        self.descriptionWeather.setTextColor(UIColor.whiteColor())
        self.descriptionWeather.setText("")
    }
    
    
    //Send Message to Parrent App in iPhone / iPad
    private func sendMessageToParentAppWithString(messageText: String) {
        let infoDictionary = ["message" : messageText]
        
        WKInterfaceController.openParentApplication(infoDictionary) {
            (replyDictionary, error) -> Void in
            
            if let castedResponseDictionary = replyDictionary as? [String: String],
                responseMessage = castedResponseDictionary["message"]
            {
                self.nameCity.setText(responseMessage)
            }
        }
    }
    
    //Handle API Request for weather Status
    private func handleAPIRequest(name : String){
        //call weather API
        var trimmedString:String = name.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        var info:String = WEATHER_API + trimmedString
        
        WeatherRequest.request(info, completion:{(dic:NSDictionary,mainDic:NSDictionary) -> Void in
            
            //set value to view
            
            self.displayWeather(dic, main: mainDic)
            
        })
    }
    
    //UserInterface
    private func displayWeather(dic : NSDictionary,main:NSDictionary){
        
        var desctiptionString : String  = dic["description"] as! String
        self.descriptionWeather.setText(desctiptionString)
        
        //icon
        var num: Int = dic["id"] as! Int
        
        switch (num) {
        case 500:
            self.imageWeather.setImage(UIImage(named: "small_rain"))
        case 800:
            self.imageWeather.setImage(UIImage(named: "sun"))
        case 801:
            self.imageWeather.setImage(UIImage(named: "cloud"))
        case 803:
            self.imageWeather.setImage(UIImage(named: "cloud"))
        case 804:
            self.imageWeather.setImage(UIImage(named: "rain"))
            
        default:
            self.imageWeather.setImage(UIImage(named: "sun"))
        }
        
        //Temp
        var tempMin : Float  = main["temp_min"] as! Float
        tempMin = convertTemperatures(tempMin, "Kelvin", "Celsius")
        var tempMinST:String = NSString(format: "%.2f C", tempMin) as String
        
        var tempMax : Float  = main["temp_max"] as! Float
        tempMax = convertTemperatures(tempMax, "Kelvin", "Celsius")
        var tempMaxST:String = NSString(format: "%.2f C", tempMax) as String
        
        var tempAve : Float  = main["temp"] as! Float
        tempAve = convertTemperatures(tempAve, "Kelvin", "Celsius")
        var tempAveST:String = NSString(format: "%.2f C", tempAve) as String
        
        //set UI
        self.tempMin.setText(tempMinST)
        self.tempMax.setText(tempMaxST)
        self.tempAverage.setText(tempAveST)
        
    }
    
}
